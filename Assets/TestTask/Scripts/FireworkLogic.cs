﻿using UnityEngine;
using System.Collections;

public class FireworkLogic : MonoBehaviour {

    public ParticleSystem[] ps;
    public float delay;
    public AudioClip soundFireWork;
    

    // Use this for initialization
    void Start() {
        StartCoroutine("ShutOff");
    }


    IEnumerator ShutOff() {
        for (int i = 0; i < ps.Length; ++i) {
            yield return new WaitForSeconds(delay);
            ps[i].Play();
            if (soundFireWork) VOICE.me.PlaySound(soundFireWork);

        }
        StartCoroutine("ShutOff");
    }
}
