﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class VOICE : MonoBehaviour,
    ITrackableEventHandler {
    public TrackableBehaviour mTrackableBehaviour;

    public AudioSource ausMusic;
    public AudioSource ausSound;


    private bool isVisible;

    public static VOICE me;

    void Start() {
        me = this;
        if (mTrackableBehaviour) {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }

    }

    public void OnTrackableStateChanged(
        TrackableBehaviour.Status previousStatus,
        TrackableBehaviour.Status newStatus) {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED) {
            // Play audio when target is found
            ausMusic.Play();
            isVisible = true;
        } else {
            // Stop audio when target is lost
            ausMusic.Stop();
            isVisible = false;
        }
    }

    public void PlaySound(AudioClip clip) {
        if (isVisible) {
            ausSound.Stop();
            ausSound.clip = clip;
            ausSound.Play();
        }
    }



}