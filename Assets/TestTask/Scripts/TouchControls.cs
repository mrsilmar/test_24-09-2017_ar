﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class TouchControls : MonoBehaviour {

    private ThirdPersonUserControl _p;
    public Animator _ator ;
    // Use this for initialization
    void Start () {
        _p = FindObjectOfType <ThirdPersonUserControl>();
    }

    public void UpArrow() {
     _p.Move(1,0);
    }

    public void DownArrow() {
     _p.Move(-1,0);
    }
  
    
    public void LeftArrow () {
        _p.Move(0,-1);
    }

    public void RightArrow () {
        _p.Move(0,1);
    }

    public void UnpressedArrow () {
        _p.Move(0,0);
    }


    public void Crouch () {
        _p.Crouch(true);
    }

    public void StopCrouch () {
        _p.Crouch(false);
    }

    

}
