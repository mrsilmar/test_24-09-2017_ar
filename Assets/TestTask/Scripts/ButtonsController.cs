﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonsController : MonoBehaviour {

    [SerializeField]
    private Animator georgAtor;
    
    
   public void PlayState(string stateToLaunch) {
         if (georgAtor) georgAtor.Play(stateToLaunch);
    }


}
