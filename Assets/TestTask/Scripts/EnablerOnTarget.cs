﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class EnablerOnTarget : MonoBehaviour, ITrackableEventHandler {
    public TrackableBehaviour mTrackableBehaviour;

    public GameObject WalkableHero;

    // Use this for initialization
    void Start () {
        WalkableHero.SetActive(false);

        if (mTrackableBehaviour) {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
    }
    
     public void OnTrackableStateChanged(
        TrackableBehaviour.Status previousStatus,
        TrackableBehaviour.Status newStatus) {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED) {
            // Play audio when target is found
           WalkableHero.SetActive(true);
        } else {
            // Stop audio when target is lost
           WalkableHero.SetActive(false);
        }
    }
}
